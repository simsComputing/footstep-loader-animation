# How to use this loader

This package supports AMD definitions and also node module definitions (i.e define() and module.exports).

You can also use this module by injecting it directly into your web page. If you do so, you will be able to access the module through the footprintLoader variable.
WARNING : This package depends on Jquery. So you should respect the dependency requirements set in package.json.

To launch the loader do the following :

```
var footstepLoader = require('footstep-loader-animation');

footstepLoader.launch();
footstepLoader.stop();
```

There are some parameters you can customize by changing the main object properties. Please refer to source code to see what you can customize. 

Example :

If I want to customer the ID of the container for the loading animation :

```
footstepLoader.container = 'my-custom-id';
footstepLoader.launch();
```

This might be quite important if ever you use this module with webpack for example and the source image is in the node_modules folder which you can't access at runtime.

If ever this happens, you are able to copy/load the image and then change the path to it as follows :

Example with Webpack :

```
var imagePath = require('footstep-loader-animation/footprint.png');
var footstepLoader = require('footstep-loader-animation');

footstepLoader.imagePath = './' . imagePath;
footstepLoader.launch();
```