(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global = global || self, global.footstepLoader = factory());
})(this, function ($) {"use strict";
    var $ = typeof window.$ !== 'undefined' ? window.$ :
    (typeof require === 'function' ? require('jquery') : null);

    if (null === $) {
        throw 'Missing Jquery dependency';
    }

    var footprintLoader = {
        container: 'loader-container',
        footprintAnimationClass: 'footprint-animation',
        loaderMessage: 'loader-message',
        loaderAnimationModal: 'loader-animation-modal',
        loaderAnimationBodyCover: 'loader-animation-body-cover',
        imagePath: './footprint.png',
        parentTagSelector: 'body',
        createCircle: function (radius, nbSteps, initialShift, idPrefix) {
            for (var i = 0; i < nbSteps; i++) {
                var deg = (360 * i / nbSteps) + initialShift;
                var template = `<img class="${this.footprintAnimationClass}" id="${idPrefix}-${i}" src="${this.imagePath}" alt="footprint" />`;
                var element = $(template);
                element.css('transform', `rotate(${deg}deg) translate(${radius}px)`);
                $(`#${this.container}`).append(element);
            }
        },
        animate: function (elem, totalTime, delay) {
            elem.css('animation', `walk-blink ${totalTime}s infinite ease-in-out`);
            elem.css('animation-delay', delay.toString() + 's');
        },
        createModal: function(message) {
            var middleScreenHeight = ($(window).height() / 2) + $(document).scrollTop() - 95;
            var middleScreenWidth = ($(window).width() / 2) + $(document).scrollLeft() - 95;
        
            var scrollTop = $(window).scrollTop();
            var scrollLeft = $(window).scrollLeft();
            $(window).on('scroll', function(e) {
                $(e.target).scrollTop(scrollTop);
                $(e.target).scrollLeft(scrollLeft);
            });
        
            var html = `
                <div style="top: ${$(document).scrollTop()}px; left: ${$(document).scrollLeft()}px" id="${this.loaderAnimationBodyCover}">
                    
                </div>
                <div style="top: ${middleScreenHeight}px; left: ${middleScreenWidth}px" id="${this.loaderAnimationModal}">
                        <div id="${this.container}" data-message="${message}">
                        </div>
                </div>
                    `;
        
            $(`${this.parentTagSelector}`).append(html);
        },
        addMessage: function () {
            var container = $(`#${this.container}`);
            var message = container.data('message');

            container.append(`<p id="${this.loaderMessage}">${message}</p>`);
        },
        launch: function(message) {
            this.createModal(message);
            this.createCircle(50, 8, 0, 'int');
            this.createCircle(60, 8, 20, 'ext');
            this.addMessage();
            
            var totalTImeForRound = 6;
            
            for (var i = 0; i < 8; i++) {
                var elem = $('#int-' + i.toString());
                var elem2 = $('#ext-' + i.toString());
            
                var delay = totalTImeForRound * (7 - i) / 8;
                this.animate(elem, totalTImeForRound, delay + totalTImeForRound / 16);
                this.animate(elem2, totalTImeForRound, delay);
            }
        },
        stop: function() {
            $(`#${this.loaderAnimationModal}`).remove();
            $(`#${this.loaderAnimationBodyCover}`).remove();
            $(window).off('scroll');
        }
    }

    return footprintLoader;
});
